from django.db import models
from django.conf import settings
from django.db import connections
from postgis_loader.models import get_layer
from django.contrib.gis.gdal import GDALRaster
from math import isnan

# basic idea:
#  ibge=> select ppas.n as ppas from (select count(id) as n from soil.ft_ppas where st_intersects(the_geom, 'SRID=31370;POINT(146546 169775 0)'::geometry)) as ppas;
#  NOTICE:  unsupported OGR FDW expression type, T_FuncExpr
#   ppas
#  ------
# 1
#  (1 row)


SOURCE_REGION = getattr(settings, "INFILTRATION_SOURCE_REGION", "urbadm.urbadm_re")
SOURCE_PPAS = getattr(settings, "INFILTRATION_SOURCE_PPAS", "soil.mv_ppas")
SOURCE_CADASTRE = getattr(
    settings, "INFILTRATION_SOURCE_CADASTRE", "base_plan.urbis_cadastre"
)
SOURCE_SOL = getattr(
    settings, "INFILTRATION_SOURCE_SOL", "risk.site_inventaire_sol_ext_public"
)
SOURCE_CAPTAGE = getattr(
    settings, "INFILTRATION_SOURCE_CAPTAGE", "water.pa_capt_zone3_area_f"
)
SOURCE_PATRIMOINE = getattr(
    settings, "INFILTRATION_SOURCE_PATRIMOINE", "soil.mv_patrimoine_class_definitif"
)
SOURCE_NAPPE = getattr(
    settings, "INFILTRATION_SOURCE_NAPPE", "groundwater.gw_hgeol_phreatic_depth"
)
SOURCE_CANAL = getattr(settings, "INFILTRATION_SOURCE_CANAL", "water.swb_kan_f_09")
SOURCE_ETANG = getattr(
    settings, "INFILTRATION_SOURCE_ETANG", "web_service.wsl_bruenvi_water_surface"
)
SOURCE_EAU_VOUTE = getattr(
    settings,
    "INFILTRATION_SOURCE_EAU_VOUTE",
    "web_service.wsl_bruenvi_water_river_cover",
)
SOURCE_EAU_OUVERT = getattr(
    settings,
    "INFILTRATION_SOURCE_EAU_OUVERT",
    "web_service.wsl_bruenvi_water_river_open",
)
SOURCE_NATURA = getattr(
    settings,
    "INFILTRATION_SOURCE_NATURA",
    "web_service.wsl_bruenvi_natura_2000_station",
)
SOURCE_RESERVE = getattr(
    settings,
    "INFILTRATION_SOURCE_RESERVE",
    "web_service.wsl_bruenvi_natural_reserve",
)
SOURCE_INONDATION = getattr(
    settings, "INFILTRATION_SOURCE_INONDATION", "flood.r_flo_alea123_19"
)
SOURCE_PAD = getattr(settings, "INFILTRATION_SOURCE_PAD", "soil.pad_tous_infos")

SOURCE_PHREATIC = getattr(
    settings,
    "INFILTRATION_SOURCE_PHREATIC",
)
SOURCE_MUNICIPALITIES = getattr(
    settings,
    "INFILTRATION_SOURCE_MUNICIPALITIES", 
    "soil.municipalities_infiltrasoil")


def schema_and_table(source):
    return source.split(".")


SQL_BASE_INTERSECTS = """
SELECT 
    result.n as result 
FROM (
    SELECT count(*) AS n 
    FROM {table} 
    WHERE st_intersects(ST_SetSRID({geometry_column}, 31370), 'SRID=31370;POINT(%s %s)'::geometry)
    ) AS result;
"""

SQL_BASE_INTERSECTS_VALUE = """
SELECT 
    result.{value} 
FROM (
    SELECT {value} 
    FROM {table} 
    WHERE st_intersects(ST_SetSRID({geometry_column}, 31370), 'SRID=31370;POINT(%s %s)'::geometry)
    ) AS result LIMIT 1;
"""

SQL_BASE_DISTANCE = """
WITH point(p) AS (values ('SRID=31370;POINT(%s %s)'::geometry))
SELECT 
    st_distance((SELECT p FROM point), ST_SetSRID({geometry_column}, 31370)) AS dist 
FROM {table} 
WHERE 
    ST_DWithin((SELECT p FROM point), ST_SetSRID({geometry_column}, 31370), {buffer})
ORDER BY 
    st_distance((SELECT p FROM point), ST_SetSRID({geometry_column}, 31370)) 
LIMIT 1;
"""


class GeometryColumnCache:
    def __init__(self):
        self._cached = {}

    def __call__(self, schema, table):
        key = f"{schema}.{table}"
        if key not in self._cached:
            model, geometry_column, geometry_type = get_layer(schema, table)
            self._cached[key] = geometry_column

        return self._cached[key]


geo_col_cache = GeometryColumnCache()


def test_intersect(schema, table):
    def inner(x, y):
        sql = SQL_BASE_INTERSECTS.format(
            table=table, geometry_column=geo_col_cache(schema, table)
        )
        if sql is None:
            return False
        connection = connections[schema]
        with connection.cursor() as cursor:
            cursor.execute(sql, [x, y])
            row = cursor.fetchone()
            value = row[0]
            return value > 0

    return inner


def test_intersect_value(schema, table, value):
    interesting_value = value

    def inner(x, y):
        sql = SQL_BASE_INTERSECTS_VALUE.format(
            table=table,
            geometry_column=geo_col_cache(schema, table),
            value=interesting_value,
        )
        connection = connections[schema]
        with connection.cursor() as cursor:
            cursor.execute(sql, [x, y])
            row = cursor.fetchone()
            if row is None:
                return None
            value = row[0]
            return value

    return inner


def test_distance(schema, table, buffer):
    def inner(x, y):
        sql = SQL_BASE_DISTANCE.format(
            table=table, geometry_column=geo_col_cache(schema, table), buffer=buffer
        )
        connection = connections[schema]
        with connection.cursor() as cursor:
            cursor.execute(sql, [x, y])
            row = cursor.fetchone()
            if row is None:
                return None
            value = row[0]
            return value

    return inner


# https://git.environnement.brussels/carto/infiltrasoils/-/issues/5
test_region = test_intersect(*schema_and_table(SOURCE_REGION))

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/9
test_ppas = test_intersect(*schema_and_table(SOURCE_PPAS))

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/10
test_patrimoine = test_intersect(*schema_and_table(SOURCE_PATRIMOINE))

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/11
test_cadastre = test_intersect(*schema_and_table(SOURCE_CADASTRE))
test_sol = test_intersect_value(*schema_and_table(SOURCE_SOL), value="category")

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/12
test_captage = test_intersect(*schema_and_table(SOURCE_CAPTAGE))

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/13
distance_canal = test_distance(*schema_and_table(SOURCE_CANAL), buffer=100)
distance_etang = test_distance(*schema_and_table(SOURCE_ETANG), buffer=100)
distance_eau_voute = test_distance(*schema_and_table(SOURCE_EAU_VOUTE), buffer=100)
distance_eau_ouvert = test_distance(*schema_and_table(SOURCE_EAU_OUVERT), buffer=100)

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/14
distance_natura = test_distance(*schema_and_table(SOURCE_NATURA), buffer=60)
distance_reserve = test_distance(*schema_and_table(SOURCE_RESERVE), buffer=60)

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/17
test_inondation = test_intersect(*schema_and_table(SOURCE_INONDATION))

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/8
test_pad = test_intersect(*schema_and_table(SOURCE_PAD))

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/18
test_rcu = test_intersect_value(*schema_and_table(SOURCE_MUNICIPALITIES), value="rcu")

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/18
test_municipal_premium = test_intersect_value(*schema_and_table(SOURCE_MUNICIPALITIES), value="primes")

# https://git.environnement.brussels/carto/infiltrasoils/-/issues/15
def world2Pixel(geoMatrix, x, y):
    """
    Uses a gdal geomatrix (gdal.GetGeoTransform()) to calculate
    the pixel location of a geospatial coordinate
    """
    ulX = geoMatrix[0]
    ulY = geoMatrix[3]
    xDist = geoMatrix[1]
    pixel = int((x - ulX) / xDist)
    line = int((ulY - y) / xDist)
    return (pixel, line)


class Raster:
    def __init__(self, path):
        self._path = path
        self._data = None
        self._transform = None

    def _load(self):
        ds = GDALRaster(self._path)
        self._transform = ds.geotransform
        self._data = ds.bands[0].data()
        ds = None

    def _with_data(self):
        if self._data is None:
            self._load()

    def at_coords(self, lng, lat):
        self._with_data()
        x, y = world2Pixel(self._transform, lng, lat)
        v = float(self._data[y, x])
        print("rast", v)
        if isnan(v):
            return 0.0
        return v


phreatic_raster = Raster(SOURCE_PHREATIC)


def test_phreatic(x, y):
    return phreatic_raster.at_coords(x, y)


class Config(models.Model):
    id = models.AutoField(primary_key=True)
    map = models.ForeignKey("api.UserMap", on_delete=models.PROTECT, related_name="+")
    created_at = models.DateTimeField(auto_now_add=True)
    current = models.BooleanField(default=False)

    def __str__(self) -> str:
        return str(self.map.title)

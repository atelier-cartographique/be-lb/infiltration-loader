from django.contrib import admin
from infiltration.models import Config


def map_title(config):
    return str(config.map.title)


class ConfigAdmin(admin.ModelAdmin):
    list_display = (
        map_title,
        "created_at",
        "current",
    )
    list_editable = ("current",)


admin.site.register(Config, ConfigAdmin)

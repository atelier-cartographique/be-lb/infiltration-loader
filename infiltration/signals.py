from django.db.models.signals import post_save
from infiltration.models import Config


def on_config_save(sender, instance, created, **kwargs):
    if instance.current:
        Config.objects.exclude(id=instance.id).filter(current=True).update(
            current=False
        )


def connect():
    post_save.connect(
        on_config_save,
        sender="infiltration.Config",
        dispatch_uid="signal_save_infiltration_config",
    )

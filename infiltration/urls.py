from django.urls import path
from infiltration.views import (
    get_config,
    get_point_data,
)


def prefix(s: str):
    return f"infiltration{s}"


urlpatterns = [
    path(
        prefix("/config/"),
        get_config,
        name=prefix(".get_config"),
    ),
    path(
        prefix("/point/<int:lon>/<int:lat>/"),
        get_point_data,
        name=prefix(".get_point_data"),
    ),
]

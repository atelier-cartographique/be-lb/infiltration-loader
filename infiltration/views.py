from django.http import JsonResponse
from infiltration.models import (
    Config,
    test_phreatic,
    test_region,
    test_rcu,
    test_municipal_premium,
    test_ppas,
    test_pad,
    test_patrimoine,
    test_cadastre,
    test_sol,
    test_captage,
    distance_canal,
    distance_etang,
    distance_eau_voute,
    distance_eau_ouvert,
    distance_natura,
    test_inondation,
    distance_reserve,
)
from infiltration.serializers import serialize_config, serialize_point_data


def get_rru(lon, lat):
    return "rru", test_region(lon, lat)

def get_regional_premium(lon, lat):
    return "regionalPremium", test_region(lon,lat)

def get_rcu(lon, lat):
    return "rcu", test_rcu(lon,lat)
    # return "rcu", True

def get_municipal_premium(lon, lat):
    return "municipalPremium", test_municipal_premium(lon, lat)
    # return "municipalPremium", True

def get_ppas(lon, lat):
    return "ppas", test_ppas(lon, lat)


def get_pad(lon, lat):
    return "pad", test_pad(lon, lat)


def get_heritage(lon, lat):
    return "heritage", test_patrimoine(lon, lat)


def get_category(lon, lat):
    if test_cadastre(lon, lat):
        cat = test_sol(lon, lat)
        if cat is not None:
            return "category", cat.replace("cat", "")
        else:
            return "category", "blanco"

    return "category", None


def get_water_capture(lon, lat):
    return "waterCapture", test_captage(lon, lat)


def get_water_surface(lon, lat):
    canal = distance_canal(lon, lat)
    etang = distance_etang(lon, lat)
    eau_voute = distance_eau_voute(lon, lat)
    eau_ouvert = distance_eau_ouvert(lon, lat)

    if canal is None and etang is None and eau_ouvert is None and eau_voute is None:
        return "surfaceWaterProx", None

    return "surfaceWaterProx", min(
        [v for v in [canal, etang, eau_ouvert, eau_voute] if v is not None]
    )


def get_natura(lon, lat):
    valn = distance_natura(lon, lat)
    valr = distance_reserve(lon, lat)
    if valn is None and valr is None:
        return "natura2000Prox", "outBuffer"        
    elif (valn is not None and valn > 0) or (valr is not None and valr > 0):
        return "natura2000Prox", "inBuffer"

    return "natura2000Prox", "inNatura"


def get_phreatic(lon, lat):
    val = test_phreatic(lon, lat)
    if val > 4:
        return "phreaticProx", False
    return "phreaticProx", True


def get_flood(lon, lat):
    return "floodAlea", test_inondation(lon, lat)


def get_point_data(request, lon, lat):
    items = [
        ('coordinates', (lon, lat)),
        get_rru(lon, lat),
        get_rcu(lon, lat),
        get_pad(lon, lat),
        get_ppas(lon, lat),
        get_heritage(lon, lat),
        get_category(lon, lat),
        get_water_capture(lon, lat),
        get_water_surface(lon, lat),
        get_natura(lon, lat),
        get_phreatic(lon, lat),
        get_flood(lon, lat),
        get_regional_premium(lon,lat),
        get_municipal_premium(lon, lat),
    ]
    return JsonResponse(serialize_point_data(dict(items)))


def get_config(request):
    config = Config.objects.get(current=True)
    return JsonResponse(serialize_config(config))

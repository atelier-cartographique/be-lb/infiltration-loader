from django.forms import ValidationError


class PointValidationError(ValidationError):
    pass


def validate_bool(name):
    def inner(value):
        if isinstance(value, bool):
            return value
        raise PointValidationError(f"{name} expected boolean, got {type(value)}")

    return inner


def validate_number(name):
    def inner(value):
        if isinstance(
            value,
            (
                int,
                float,
            ),
        ):
            return value
        raise PointValidationError(f"{name} expected number, got {type(value)}")

    return inner


def validate_union(name, values):
    def inner(value):
        if value in values:
            return value
        raise PointValidationError(f"{name} expected one of {values}, got {value}")

    return inner


def nullable(v):
    def inner(value):
        if value is None:
            return None
        return v(value)

    return inner


validate_rru = validate_bool("rru")
validate_rcu = validate_bool("rcu")
validate_pad = validate_bool("pad")
validate_ppas = validate_bool("ppas")
validate_heritage = validate_bool("heritage")
validate_category = nullable(
    validate_union(
        "category", ["blanco", "0", "1", "2", "3", "4", "0+1", "0+2", "0+3", "0+4"]
    )
)
validate_water_capture = validate_bool("waterCapture")
validate_surface_water_prox = nullable(validate_number("surfaceWaterProx"))
validate_natura2000_prox = validate_union(
    "natura2000Prox", ["inBuffer", "outBuffer", "inNatura"]
)
validate_phreatic_prox = nullable(validate_bool("phreaticProx"))
validate_flood_alea = validate_bool("floodAlea")
validate_regional_premium = validate_bool("regionalPremium")
validate_municipal_premium = validate_bool("municipalPremium")


def serialize_point_data(data):
    return {
        "coordinates": data['coordinates'],
        "rru": validate_rru(data["rru"]),
        "rcu": validate_rcu(data["rcu"]),
        "pad": validate_pad(data["pad"]),
        "ppas": validate_ppas(data["ppas"]),
        "heritage": validate_heritage(data["heritage"]),
        "category": validate_category(data["category"]),
        "waterCapture": validate_water_capture(data["waterCapture"]),
        "surfaceWaterProx": validate_surface_water_prox(data["surfaceWaterProx"]),
        "natura2000Prox": validate_natura2000_prox(data["natura2000Prox"]),
        "phreaticProx": validate_phreatic_prox(data["phreaticProx"]),
        "floodAlea": validate_flood_alea(data["floodAlea"]),
        "regionalPremium": validate_regional_premium(data["regionalPremium"]),
        "municipalPremium": validate_municipal_premium(data["municipalPremium"]),
    }


def serialize_config(config):
    return {"mapId": config.map.id}

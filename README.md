## Geothermie-loader

# Data

The infiltration-loader loads a lot of data for use in the infiltration application. Those data, stored in tables, are provided by Bruxelles-Environnement. There is also one raster file used to get the phreatic depht.

The sources of the data are set in the django settings like this:

```
INFILTRATION_SOURCE_REGION = "urbadm.urbadm_re"
INFILTRATION_SOURCE_PPAS = "soil.mv_ppas"
INFILTRATION_SOURCE_CADASTRE = "base_plan.urbis_cadastre"
INFILTRATION_SOURCE_SOL = "risk.site_inventaire_sol_ext_public"
INFILTRATION_SOURCE_CAPTAGE = "water.pa_capt_zone3_area_f"
INFILTRATION_SOURCE_PATRIMOINE = "soil.mv_patrimoine_class_definitif"
INFILTRATION_SOURCE_NAPPE = "groundwater.gw_hgeol_phreatic_depth"
INFILTRATION_SOURCE_CANAL = "water.swb_kan_f_09"
INFILTRATION_SOURCE_ETANG = "web_service.wsl_bruenvi_water_surface"
INFILTRATION_SOURCE_EAU_VOUTE = "web_service.wsl_bruenvi_water_river_cover"
INFILTRATION_SOURCE_EAU_OUVERT = "web_service.wsl_bruenvi_water_river_open"
INFILTRATION_SOURCE_NATURA = "web_service.wsl_bruenvi_natura_2000_station"
INFILTRATION_SOURCE_INONDATION = "flood.r_flo_alea123_19"

INFILTRATION_SOURCE_PHREATIC = "/home/sdi/phreatic_depht_normtopolidar_rbc.tif"
```

Default sources are defined in `models.py`.
